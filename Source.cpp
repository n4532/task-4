#include <iostream>
#include <fstream>

using namespace std;

typedef double(*function)(double);
typedef double(*formula)(double, double, unsigned, function);

double trapezoidFormula(double, double, unsigned, function);
double simpsonFormula(double, double, unsigned, function);
double integralFunction(double);
double count(formula, function, double, double, unsigned, double);

int main() {
	double beginSegm, endSegm, integral = 0, epsilon;
	unsigned n;

	ofstream fin;
	fin.open("text.txt");

	if (!fin.is_open()) {
		cout << "Could not open the file";
		return 0;
	}

	while (true)
	{
		cout << "Enter the value of regs of segment, accuracy and partitions number: " << endl;
		cin >> beginSegm;
		cin >> endSegm;
		cin >> epsilon;
		cin >> n;
		if ((beginSegm <= endSegm) && (n > 0) && (epsilon < 1) && (epsilon > 0))
			break;
	}

	system("cls");

	integral = count(trapezoidFormula, integralFunction, beginSegm, endSegm, n, epsilon);

	fin << "Result of this Integral by trapezoid formula is ";
	fin << integral << endl;

	integral = count(simpsonFormula, integralFunction, beginSegm, endSegm, n, epsilon);

	fin << "Result of this Integral by Simpson's formula is ";
	fin << integral << endl;

	fin.close();
	return 0;
}

double count(formula trapezoidFormula, function integralFunction, double beginSegm, double endSegm, unsigned n, double epsilon)
{
	double previosIntegral = 0, nextIntegral = 1;

	while (abs(previosIntegral - nextIntegral) > epsilon)
	{
		previosIntegral = trapezoidFormula(beginSegm, endSegm, n, integralFunction);
		nextIntegral = trapezoidFormula(beginSegm, endSegm, 2 * n, integralFunction);
		n = 2 * n;
	}

	return nextIntegral;
}

double trapezoidFormula(double lowerLimit, double upperLimit, unsigned k, function integralFunction)
{
	double step = (upperLimit - lowerLimit) / k;
	double integral = integralFunction(lowerLimit) + integralFunction(upperLimit);
	double temp = lowerLimit + step;

	for (int i = 1; i < k; i++) {
		integral += 2 * integralFunction(temp);
		temp += step;
	}

	double multiplier = step / 2;
	integral *= multiplier;

	return integral;
}

double simpsonFormula(double lowerLimit, double upperLimit, unsigned k, function integralFunction)
{
	double step = (upperLimit - lowerLimit) / k;
	double integral = integralFunction(lowerLimit) + integralFunction(upperLimit);
	double firstPart = 0;
	double secondPart = 0;
	double tempFirst = lowerLimit + 2 * step;
	double tempSecond = lowerLimit + step;

	for (int i = 1, j = 2; i < k; i++, j++) {
		if (j < k - 1) {
			firstPart += integralFunction(tempFirst);
			tempFirst += step;
		}
		secondPart += integralFunction(tempSecond);
		tempSecond += step;
	}

	integral += 2 * firstPart + 4 * secondPart;

	double multiplier = (upperLimit - lowerLimit) / (6 * k);
	integral *= multiplier;

	return integral;
}

double integralFunction(double x) {
	return exp(x);
}

//double taylorExp(double x, double accuracy)
//{
//
//	double sum = 1, term = 1;
//	int i = 1;
//
//	while (abs(term) > accuracy)
//	{
//		term *= x / i++;
//		sum += term;
//	}
//
//	return sum;
//}